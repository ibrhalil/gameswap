package com.jia.GameSwap.model;

import java.util.Calendar;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User 
{
	@Id
	@Column(name = "user_id")
	private UUID id;
	
	@Column(name = "user_nick_name")
	private String nickName;
	
	@Column(name = "user_password")
	private String password;
	
	@Column(name = "user_name")
	private String name;
	
	@Column(name = "user_last_name")
	private String lastName;
	
	@Column(name = "user_create_date")
	private Calendar createDate;
	
	@Column(name = "user_coin")
	private double coin;
	
}
