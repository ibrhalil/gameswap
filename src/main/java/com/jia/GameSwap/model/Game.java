package com.jia.GameSwap.model;

import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Game {
	
	@Id
	@Column(name = "game_id")
	private UUID id;
	
	@Column(name= "game_name")
	private String name;
	
	@Column(name= "game_price")
	private Double price;
	
	@Column(name="game_create_date")
	private Calendar createDate;
	
	//@Column(name="game_tag_name")
	//private List<String> tagName;
	
	

}
