package com.jia.GameSwap.service;

import java.util.List;
import java.util.UUID;

import org.springframework.stereotype.Service;

import com.jia.GameSwap.dto.GameDTO;
import com.jia.GameSwap.model.Game;
@Service
public interface GameService {
	public List<Game> getListGame();
	
	public Game getGame(UUID id);  // UUID mi olacak yoksa String mi olacak ?

	public Game save(GameDTO gameDTO);
}
