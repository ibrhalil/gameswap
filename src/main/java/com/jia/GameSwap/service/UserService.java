package com.jia.GameSwap.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.jia.GameSwap.dto.UserDTO;
import com.jia.GameSwap.model.User;
@Service
public interface UserService 
{
	public List<User> getListUser();
	
	public User getUser(String nickName);

	public void save(UserDTO userDTO);
}
