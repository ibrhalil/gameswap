package com.jia.GameSwap.service;

import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jia.GameSwap.dto.UserDTO;
import com.jia.GameSwap.model.User;
import com.jia.GameSwap.repository.UserRepository;
@Service
public class UserServiceImp implements UserService
{
	@Autowired
	private UserRepository userRepository;
	
	@Override
	public List<User> getListUser() 
	{
		return userRepository.findAll();
	}

	@Override
	public User getUser(String nickName) 
	{
		return userRepository.getByNickName(nickName);
	}

	@Override
	public void save(UserDTO userDTO) 
	{
		User user = new User();
		user.setName(userDTO.getName());
		user.setLastName(userDTO.getLastName());
		user.setCoin(0.0);
		user.setCreateDate(Calendar.getInstance());
		user.setNickName(userDTO.getNickName());
		user.setPassword(userDTO.getPassword());
		user.setId(UUID.randomUUID());
		
		userRepository.save(user);
	}

}
