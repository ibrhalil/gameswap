package com.jia.GameSwap.service;

import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jia.GameSwap.dto.GameDTO;
import com.jia.GameSwap.dto.UserDTO;
import com.jia.GameSwap.model.Game;
import com.jia.GameSwap.repository.GameRepository;
@Service
public class GameServiceImp implements GameService {

	@Autowired
	private GameRepository gameRepository;

	@Override
	public List<Game> getListGame() {
		return gameRepository.findAll();
	}

	@Override
	public Game getGame(UUID id) {
		
		return gameRepository.getById(id);
	}

	@Override
	public Game save(GameDTO gameDTO) {
		Game game = new Game();
		game.setId(UUID.randomUUID());
		game.setName(gameDTO.getName());
		game.setCreateDate(Calendar.getInstance());
		game.setPrice(gameDTO.getPrice());
		Game newGame=gameRepository.save(game);
		return newGame;
	}

}
