package com.jia.GameSwap.controller;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jia.GameSwap.dto.GameDTO;
import com.jia.GameSwap.model.Game;
import com.jia.GameSwap.service.GameService;

@RestController
@RequestMapping("/api/game")
public class GameController {
	
	@Autowired
	private GameService gameService;
	
	@GetMapping("/all")
	public List<Game> getListGame(){
		return gameService.getListGame();
	}
	
	@GetMapping("/{game-id}")
	public Game getGames(@PathVariable("game-id") UUID id) {
		return gameService.getGame(id);
	}
	
	@PostMapping("/add")
	public Game addGame(@RequestBody GameDTO gameDTO) {
		return gameService.save(gameDTO);
	}
}
