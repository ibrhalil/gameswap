package com.jia.GameSwap.repository;


import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.jia.GameSwap.model.Game;

@Transactional
public interface GameRepository  extends JpaRepository<Game, UUID>{
	
}
