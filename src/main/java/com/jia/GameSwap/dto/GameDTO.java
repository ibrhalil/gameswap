package com.jia.GameSwap.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GameDTO {
	
	private String name;
	
	private Double price;
	
	//private List<String> tagName;
}
