package com.jia.GameSwap.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jia.GameSwap.dto.GameDTO;
import com.jia.GameSwap.dto.UserDTO;
import com.jia.GameSwap.model.Game;
import com.jia.GameSwap.model.User;
import com.jia.GameSwap.service.UserService;

@RestController
@RequestMapping("/api/user")
public class UserController 
{
	@Autowired
	private UserService userService;
	
	@GetMapping("/all")
	public List<User> getListUser() 
	{
		return userService.getListUser();
	}
	
	@GetMapping("/{username}")
	public User getUser(@PathVariable("username") String nickName) 
	{
		return userService.getUser(nickName);
	}
	
	@PostMapping("/add")
	public void addGame(@RequestBody UserDTO userDTO) 
	{
		userService.save(userDTO);
	}
}
