package com.jia.GameSwap.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.jia.GameSwap.model.User;

@Transactional
public interface UserRepository extends JpaRepository<User, UUID>
{
	public User getByNickName(String nickName);
}
